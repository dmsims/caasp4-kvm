#!/bin/bash
#################################################################
# caaspinstRC.sh by Brent Griggs V.99 Oct 9 2018		#
# Easily portable RC to source and manage the install of CaaSP	#
# lab envirnonments in KVM.					#
# Example							#
# $ source ./caaspinstRC.sh                                     #
# $ installadmin #Unmanaged install of admin node		#
# $ installmaster #automated install of first master node	#
# $ installworker1 #automated install of first worker node	#
# $ installworker2 #automated install of second worker node	#
# $ installworker3 #automated install of third worker node	#
#								#
# Minimum install is 1 master and 2 workers			#
#                                                               #
# Minor mods by Dwain Sims for CaaSP 4.0.  Aug 2019             #
#################################################################
#hostnames
admin=c4admin
master=c4master1
worker1=c4worker1
worker2=c4worker2
worker3=c4worker3
worker4=c4worker4
#MacAddresses
adminmac=52:54:00:44:c4:99
mastermac=52:54:00:44:c5:91
worker1mac=52:54:00:44:c5:11
worker2mac=52:54:00:44:c5:12
worker3mac=52:54:00:44:c5:13
worker4mac=52:54:00:44:c5:14
#LabEnvironment
vmname=caasp4
network=caaspnet

#
# The variable "disk" specifies the directory where the VMs disk images will be stored.
#
#disk=/data/kvm/
disk=/vmstore
dnsdom=caasplab.com
#
# Location of the SLE 15 SP1 iso for installation of CaaSP 4.0
#
caaspiso=/home/dsims/isos/SLE-15-SP1-Installer-DVD-x86_64-GM-DVD1.iso
#
# "ayastnfshost" and "aystnfspath" specify where the autoyast files will be stored.
# The defualt is to use the KVM host as the NFS server and /auto as the path where the files are stored.
#
ayastnfshost="caasphost.caasplab.com"
#ayastnfshost="c4admin.caasplab.com"
ayastnfspath="auto"

#
# These variables specify the amount of RAM allocated to the master and worker nodes.  With CaaSP 4.0 you can take the workers
# down to as low as 2GB of RAM.  On a 32 GB KVM host there is plenty of room for a master of 8GB and workers with 4GB.
#
adminmem=4096
mastermem=8192
workermem=6144


alias installadmin='vm=$vmname$admin; virt-install  --connect qemu:///system --virt-type kvm --vcpus 2 --os-variant sle15sp1 --name $vm --memory $adminmem --disk $disk/$vm.qcow2,bus=virtio,format=qcow2,size=40 --location $caaspiso --graphics vnc --network network=$network,model=virtio,mac=$adminmac --extra-args "netsetup=dhcp autoyast=nfs://$ayastnfshost$ayastnfspath/adminautoyast.xml hostname=$admin"&'

alias installmaster='vm=$vmname$master;  virt-install  --connect qemu:///system --virt-type kvm --vcpus 4 --os-variant sle15sp1 --name $vm --memory $mastermem --disk $disk/$vm.qcow2,bus=virtio,format=qcow2,size=40 --location $caaspiso --graphics vnc --network network=$network,model=virtio,mac=$mastermac --extra-args "netsetup=dhcp autoyast=nfs://$ayastnfshost/$ayastnfspath/autoyast.xml hostname=$master"&'

alias installworker1='vm=$vmname$worker1;  virt-install  --connect qemu:///system --virt-type kvm --vcpus 4 --os-variant sle15sp1 --name $vm --memory $workermem --disk $disk/$vm.qcow2,bus=virtio,format=qcow2,size=80 --location $caaspiso --graphics vnc --network network=$network,model=virtio,mac=$worker1mac --extra-args "netsetup=dhcp autoyast=nfs://$ayastnfshost/$ayastnfspath/autoyast.xml   hostname=$worker1"&'

alias installworker2='vm=$vmname$worker2;  virt-install  --connect qemu:///system --virt-type kvm --vcpus 4 --os-variant sle15sp1 --name $vm --memory $workermem --disk $disk/$vm.qcow2,bus=virtio,format=qcow2,size=80 --location $caaspiso --graphics vnc --network network=$network,model=virtio,mac=$worker2mac --extra-args "netsetup=dhcp autoyast=nfs://$ayastnfshost/$ayastnfspath/autoyast.xml   hostname=$worker2"&'

alias installworker3='vm=vmname$worker3;  virt-install  --connect qemu:///system --virt-type kvm --vcpus 4 --os-variant sle15sp1 --name $vm --memory $workermem --disk $disk/$vm.qcow2,bus=virtio,format=qcow2,size=80 --location $caaspiso --graphics vnc --network network=$network,model=virtio,mac=$worker3mac --extra-args "netsetup=dhcp autoyast=nfs://$ayastnfshost/$ayastnfspath/autoyast.xml   hostname=$worker3"&'

alias installworker4='vm=vmname$worker4;  virt-install  --connect qemu:///system --virt-type kvm --vcpus 4 --os-variant sle15sp1 --name $vm --memory $workermem --disk $disk/$vm.qcow2,bus=virtio,format=qcow2,size=80 --location $caaspiso --graphics vnc --network network=$network,model=virtio,mac=$worker4mac --extra-args "netsetup=dhcp autoyast=nfs://$ayastnfshost/$ayastnfspath/autoyast.xml   hostname=$worker4"&'

#sudo virsh net-start caaspnet
