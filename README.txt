﻿CaaSP-in-a-Box

This was originally stolen from Brent Griggs.
And then heavily modified to work with SUSE Container as a Service Platform Version 4.

Pre-reqs:

1.  A system (laptop or server) that is capable of running KVM and several virtual machines at the same time.  32 GB is likely the  bare minimum.  This has worked well on both Leap 42.3 and SLES 12 SP3 and SP4. (Your mileage may vary with other OS versions)

2. A copy of the SLE 15 SP 1 Installer.   I have been using SLE-15-SP1-Installer-DVD-x86_64-GM-DVD1.iso with good luck.  (I pulled it off of kirk.provo.novell.com).

3. Set up KVM. With the CaaSP 3.x version of this tutorial, you needed an SMT server to get the updates from.  This is not necessary with CaaSP 4.x, as the unified installer of SLE 15 SP 1 allows a CaaSP node to be one of the installation targets. You will, however, need an account on SUSE Customer Center and access to a CaaSP Reg Code. 

4. You will also need a web browser on demo system so that you can interact with the demos running on CaaSP 4.0.    Firefox worked well for me.

5. You can demo the Kubernetes cluster from the kvm host if you have kubectl installed.  BEWARE, however, that the version of kubectl matches (or is reasonably close to) the version of Kubernetes that is running in the cluster.  If these versions are way out of sync, you will see things fail that should work, and strange error messages that leave little clue to the actual issue.  CaaSP 4.0 is installing Kubernetes version 1.15 as of first shipment (and CaaSP 4.1 is installing Kubernetes 1.16.2).  Running any configuration or demos is best done from the admin node that is set up as part of this tutorial. [For example, while developing this procedure I used a KVM host running SLES 12 SP3.  The kubectl version on that distro is 1.9 and I was trying to make it work with Kubernetes 1.15 in CaaSP 4.0.  It worked fine in some areas but failed miserably in others. And when it failed, it was hard to figure out what was going on from the error messages.] 


Overview

What you are basically going to do is install CaaSP into several KVM virtual machines.  With CaaSP 3.x I found that I could only bring up one admin, one master, and two workers reliably on a 32 GB laptop. However, with CaaSP 4.x, things seem to be much lighter in resource requirements, so it is possible to have an admin node, a master node and four worker nodes.  If your goal is to show an install demo along with basic interaction with the cluster, two workers are totally sufficient.


In short, this demo contains a handy script that sets a bunch of alias’ for you to simplify the launching of the VMs into KVM.  This is the caaspinstRC.sh file used in the step below.  To understand what is happening, source the script and run “alias | grep install”.  There are alias’ defined to launch the admin, master, and worker nodes.  These aliased commands depend on autoyast files, one for the admin node, and one for the Kubernetes cluster nodes.  These autoyast files make it easy to install the CaaSP nodes without a lot of interaction.  You may want to make modifications to these autoyast files for your situation;  this will make subsequent installs of CaaSP easier.  More on this later.  Using autoyast helps us mimic the way the CaaSP 3.0 installation worked with CaaSP 4.x.

You also have to define your virtual network before you start launching any VMs.

A new addition to the CaaSP-in-Box for CaaSP 4.0 are two new features that make the Kubernetes cluster much more functional than we had with CaaSP 3.0.  Part of the cluster configuration involves the addition of Metal Load Balancer; this will allow deployments to be assigned IP addresses and allow you to interact via browser with the running pods.  Another new feature is NFS based storage for the Kubernetes cluster.  The KVM host will serve as the NFS server for the cluster. 

Also to note, I ran the VM creation commands as root (steps 4, 7, 11, and 12+).  You can likely do magic with sudo and be safer, but this is a demo box and brute force is OK with me.

Steps:

0) Log into the KVM host, and clone the contents of this repo from GitLab

	$ git clone https://gitlab.com/dmsims/caasp4-kvm.git 
	
This will create a directory called "caasp4-kvm".  Change directories (cd) into this directory.  Here you will find all of the scripts and config files needed to set up CaaSP 4.

1) Define the network with (as root)

	# virsh net-define caaspnet-dhcp-4.xml

	Note:  This xml file defines a 172.25.0.x network that works OK for me.  You may need to change for your situation.  This network is called “caaspnet”.  If you use a different network range, there are some other places that you will need to make changes in some files.  These will be pointed out as we go.

Also, start the virtual network.

      #  virsh net-start caaspnet

2) Source the environment variables and alias command used to quickly install the VMs with (as root))

	#  source caaspinstRC.sh

	Note:  You will need to edit the caaspinstRC.sh file to define the location of your CaaSP ISO image.  “caaspiso” is the variable that you will need to change here.

3) The installation aliases use autoyast files that are shared via NFS.  Just as we will do with storage for the cluster, we are using the KVM host as the NFS server.  The scripts assume that the NFS share is /auto and the base autoyast files will need to be copied to this directory before you can build any KVM VMs.  

4) Manually create (as root) two directories.  "/cstore" (for the Kubernetes cluster storage) and "/auto" for the autoyast files to reside.  The needed space for /auto is only likely to be 1o MB or less.  /cstore, on the other hand, could use a lot of space, depending on what you are deploying to the Kubernetes cluster.  For most demo purposes, 1 GB is likely sufficient.

5) Run "yast" on the KVM host and start up NFS Service configuration wizard.  Share the two directories ("/cstore and /auto") and open them for all hosts (likely OK for this demo).  Options should be (rw,no_root_squash,sync,no_subtree_check,no_all_squash).

5a)  You may need to add NFS Server capabilities to your KVM host, so be aware.

6) Edit the sample autoyast file, adminautoyast.xml.  

6a)  Looking at the adminautoyast.xml file, there are a couple of changes you may want to make. The CaaSP 4.0 documentation assumes that you will be working with a common user id called "sles" for your installs.  The sample adminautoyast.xml file defines this user as part of the install of the admin node.  If you have a password you would like to use, insert the salted hash of it where you find "<user_password>". [Note:  I used the same password as my standard user login for my KVM host just to make things easier for me.] [Note 2: To generate a password hash, use the command "openssl passwd -1 -salt <sometext> <securepassword>".  This will give you a hash that you can stick in the autoyast file.] 

6b) This is also around the same place you can add your drop in your public key for your user on your KVM host, in a stanza called <authorized_keys>.  This makes logging into your admin server easy from the KVM host.  

6c) Add your own reg_code to the autoyast file.  If you a SUSE employee, you can get a Internal Use key (for CaaSP) from your normal sources.  If you are a partner or customer of SUSE you can get a trial key from the SUSE Web Site.  "reg_code" is specified in the "suse_register" section of the adminautoyast.xml file.  Also change the "email" specification in this section to your email that is registered with SUSE Customer Center.

6d) You can also change the timezone in the autoyast file, if needed.

6e) Copy the edited adminautoyast.xml file to /auto on the KVM host.

7) At this point, install the admin node. Install the Admin node invoking the alias 'installadmin'. This should be fully automated.

7a) When you pulled this kit from gitlab, you will see two stub files created, id_rsa and id_rsa.pub.  These are placeholders for private and public keys for the sles@c4admin user.  You can replace these with the keys that you should create manually on the c4admin node (step 7b).  Ideally, create a directory "local" in your working directory, and move the edited files there.  Git will not mess with files in the "local" directory.  The script used in step 8, "newadmin" will look first for a "local" directory and pull the files from there, and then look in the current directory.

7b) When you first time set up the admin node, login from the KVM host

	$  ssh sles@c4admin
	(you may have to accept keys and entries into known_hosts at this point)

    Then generate your public and private keys

	$  ssh-keygen -t rsa

    This will create keys in /home/sles/.ssh.

	$ ls ~/.ssh
	authorized_keys  id_rsa   id_rsa.pub


7c) You can copy  (via scp) these keys back to your working install directory on the KVM host (in the "local" directory)  where the CaaSP install is staged to replace the stub files from the git clone.   This way, when you do step 8 (on subsequent installs of CaaSP) you will keep the same keys each time.

7d) You	will use the id_rsa.pub file in step 9a (below).

8) From the KVM host, run the script called "newadmin".  [Do this from your regular KVM host user, not root.]  This will copy all of the pertinent files and keys to the newly created admin node.  If you copied your keys when you built your first admin node, "newadmin" will copy them back to any new admin node that you might make.

8a) You should not need to create a new admin node every time that you install CaaSP 4.x.  If you mess up your CaaSP cluster during demos, or new beta releases come out, you can just delete the master and worker nodes and start over at step 11 in this procedure.  

9) Copy the the template autoyast.xml file to /auto.  Edit the /auto/autoyast.xml file for the Kubernetes cluster nodes.

9a) Near the stanza where the "sles" user is defined you will find a section here for authorized keys.  Add the public key (i.e. rsa_id.pub) (step 7b) for your user for the KVM host, and the public key for the sles user on your admin node.  This will make logging into the cluster nodes very easy if you need to do any config or administration.  

9b) Add your own reg_code to the autoyast file.  If you a SUSE employee, you can get a Internal Use key (for CaaSP) from your normal sources.  If you are a partner or customer of SUSE you can get a trial key from the SUSE Web Site.  "reg_code" is specified in the "suse_register" section of the autoyast.xml file.  Also change the "email" specification in this section to your email th
at is registered with SUSE Customer Center.

10) Manually add host entries to /etc/hosts for the admin and master servers on your KVM host system.

10a) Make sure that you can ping c4admin from the KVM host.

10b) From the KVM host you should be able to login to the sles account on the c4admin VM.

	$  ssh sles@c4admin

11) Back on the KVM host (as root), install the master with the alias 'installmaster'.

12) Install the first worker with the alias 'installworker1'.

13) Install the second worker with the alias 'installworker2'.

14) If you want to demo more workers, install them with "installworker3" and "installworker4".  You need two worker nodes at a minimum.

15) Wait until the installs are completed.  As a test, see if you can ping the master and workers from the c4admin node.

16) From the admin node (c4admin) while logged in as sles, run the script "buildcluster".  (this was copied to the admin node by the "newadmin" script. (In step 8))

	$  ssh sles@c4admin
	$  buildcluster

    If you created more than two workers, specify how many on the command line.

	$  buildcluster 4

   "buildcluster"  executes the skuba commands that will initialize and bootstrap the cluster.  Be aware that "buildcluster" takes a few minutes to run.  The more workers, the longer it will take.

17) If you made changes to the network range back in step 1, this is where you need to be aware of those changes.  On the admin node, cd into the caaspnetworking directory and modify the file "layer2-metallb.yaml" file for your network and/or IP address range you want to Metal LB to assign to Kubernetes services. 

18) It is a really good idea to give the cluster that you built in step 16 a chance to settle down before you run "configcluster."  There is a lot going on during the bootstrap, so wait 4-5 minutes before running the configcluster.  If you are running "Virtual Machine Manager" on the KVM host you can see when the master and worker nodes settle down into a steady state from the performance graphs.

19) From the admin node (c4admin) while logged in as sles, run the script "configcluster".  (this was copied to the admin node by the "newadmin" script.

	$ configcluster

   This script should take care of everything else.  It will copy your kubectl keys to ~/.kube. It will install Tiller.   It will set up Metal LB as a load balancer for the cluster.  It will install the nfs-client provisioner (using the KVM Host as the NFS server). And it will install the Stratos Console.

20) If you want to run kubectl commands from the KVM host, you will need to copy the kube config file back to the KVM host in the home directory of your primary login.  From the KVM host run

	$  scp sles@c4admin:~/.kube/config ~/.kube/config

Test this by running from the KVM host...

	$ kubectl get nodes

And you should see the cluster nodes that you have created.

21) Output at the end of the configcluster script should show the IP address for the Stratos Console.  From the KVM host or the Admin node, just point your browser at https://<IP Address> to get to the login screen of the Console.


22) Connect to the IP address of the Stratos instance via https://<IP>.  If you kept your stratos-values-small.yaml file unmodified, your login/password is admin/tux.

23) Connect to your CaaSP installation as an endpoint by clicking on "+" icon in Stratos.

24) You will need to use the FQDN of the Kubernetes master node as the endpoint.  You can see this by running

	$ kubectl cluster-info

Be sure to check the box "Skip SSL Validation for the Endpoint."

25)  Once you are bootstrapped and configured, you can show the cluster working in very functional fashion.

You can then run some kubectl commands.

From the KVM host run or the Admin node run.... 

kubectl cluster-info
kubectl get nodes
kubectl get pods 
kubectl get namespaces

26) A quick command to show networking functionality is to run the Nginx Hello container.

	$ kubectl create namespace <YOURSPACENAME>
	$ kubectl run hello-app --image=nginxdemos/hello --port=80 --replicas=2 -n <YOURSPACENAME>
	$ kubectl expose rc hello-app --port=80 --type="LoadBalancer" -n <YOURNAMESPACE>
	$ kubectl get rc -n <YOURNAMESPACE>

   "rc" is replication controller, a resource type in Kubernetes

27) Find the IP address assigned.

	$ kubectl get services -n  <YOURNAMESPACE>

28) Once you have the IP address (it will likely be 172.25.0.231), hit that address from the browser on your KVM host.  You should   get a nice "Welcome to Nginx" page.

29) For real fun, run the Microsoft SQL Server on Linux Container demo.  The yaml file for this has been copied to the admin node.

	$ kubectl apply -f ./sqldeployment-nfs.yaml -n <YOURNAMESPACE>

   This should deploy a pod, a service (external IP) and a persistent volume claim.

30)  Get the IP address of the service....

	$ kubctl get services -n <YOURNAMESPACE>

31)  For even more fun, install azuredatastudio on your KVM host and log into SQLserver.  There is more info on how to do this in the document, SQL-instructions.txt, copied to your admin node.

32) As newer versions of CaaSP 4.0 are released, you should be able to delete your master and worker and recreate them starting at step 11.  You should not need to build a new admin node everytime.  But if you do decide to build a new admin node, try to preserve the id_rsa and id_rsa.pub files that you created in step 7.  This will make your new installs much faster and easier. 

33) There is now included a "clearcluster" script that you can run from the KVM host (as root) and it will stop and wipe away your current running CaaSP 4 Cluster.  This makes setting up a new beta or GMC easy.

34) Once you have edited your adminautoyast.xml and autoyast.xml files in /auto on the KVM host, you should not need to make changes to them on subsequent CaaSP installs.  The keys, reg-codes, and password hashes should already be in place for the next iteration of Cluster building.

Dwain Sims
dwain.sims@suse.com
